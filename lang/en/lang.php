<?php return [
    'plugin' => [
        'name' => 'Maps',
        'description' => 'Maps',
    ],
    'permissions' => [
        'label' => 'Manage Maps',
        'tab' => 'Maps',
    ],
    'menu' => [
        'locations' => 'Locations',
        'settings' => 'Settings',
    ],
    'general' => [
        'view_more' => 'View more',
    ],
    'fields' => [
        'settings' => [
            'map_marker_icon' => 'Marker Icon',
            'map_marker_clusterer_icon' => 'Marker Clusterer Icon',
            'map_marker_clusterer_icon_x_offset' => 'Marker Clusterer Icon - X Offset',
            'map_marker_clusterer_icon_y_offset' => 'Marker Clusterer Icon - Y Offset',
            'map_enable_spiderifier' => 'Enable Spiderifier',
            'google_maps_api_key' => 'Google Maps API Key',
            'mapbox_api_key' => 'Mapbox API Key',
            'mapbox_style_url' => 'Mapbox Style URL',
            'mapbox_version' => 'Mapbox Version',
        ],
        'tabs' => [
            'general' => 'General',
            'google' => 'Google',
            'mapbox' => 'Mapbox',
        ],
        'locations' => [
            'title' => 'Location settings',
            'model_type' => 'Model type',
            'popup_field' => 'Popup field',
            'excluded_ids' => 'Excluded IDs',
            'excluded_ids_comment' => 'IDs to be excluded from the front-end. Comma-separated.',
            'detail_page_url' => 'Detail page url',
            'detail_page_url_comment' => 'Start with \'/\'. Use [id] to replace with the model\'s ID.',
            'view_page_label' => 'View page label',
            'overwrite_fields' => 'Overwrite fields',
            'overwrite_fields_add' => 'Add new field',
            'overwrite_fields_field' => 'Field',
            'overwrite_fields_type' => 'Type',
            'overwrite_fields_type_placeholder' => 'Select a type',
            'overwrite_fields_name' => 'Name',
            'overwrite_fields_latitude' => 'Latitude',
            'overwrite_fields_longitude' => 'Longitude',
            'overwrite_fields_city' => 'City',
            'overwrite_fields_zip_code' => 'ZIP code',
            'tabs' => [
                'general' => 'General',
            ],
            'name' => 'Name',
            'slug' => 'Slug',
            'location' => 'Location',
            'geodata' => 'Geodata',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'city' => 'City',
            'zip_code' => 'ZIP Code',
            'opening_times' => 'Opening Times',
            'opening_day' => 'Opening Day',
            'opening_other' => 'Other',
            'active' => 'Active (visible on site)',
        ],
    ],
    'columns' => [
        'model_type' => 'Model Type',
    ],
    'components' => [
        'locations' => [
            'model_id' => 'Model ID',
            'location_id' => 'Location ID',
            'location_id_validation' => 'Only numbers are allowed.',
            'location_id_placeholder' => 'Select a location',
            'disabled_search' => 'Disabled search',
            'refresh_markers_after_filter' => 'Refresh markers after filter',
        ],
    ],
];
