<?php namespace Key\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKeyMapsLocationModels3 extends Migration
{
    public function up()
    {
        Schema::table('key_maps_location_models', function($table)
        {
            $table->string('detail_page_url', 100)->after('popup_field')->nullable();
            $table->string('view_page_label', 100)->after('popup_field')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('key_maps_location_models', function($table)
        {
            $table->dropColumn('detail_page_url');
            $table->dropColumn('view_page_label');
        });
    }
}
