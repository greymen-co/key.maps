<?php namespace Key\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKeyMapsLocationModels2 extends Migration
{
    public function up()
    {
        Schema::table('key_maps_location_models', function($table)
        {
            $table->string('popup_field', 100)->after('model_type')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('key_maps_location_models', function($table)
        {
            $table->dropColumn('popup_field');
        });
    }
}
