<?php namespace Key\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKeyMapsLocationModels extends Migration
{
    public function up()
    {
        Schema::table('key_maps_location_models', function($table)
        {
            $table->text('overwrite_fields')->after('model_type')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('key_maps_location_models', function($table)
        {
            $table->dropColumn('overwrite_fields');
        });
    }
}
