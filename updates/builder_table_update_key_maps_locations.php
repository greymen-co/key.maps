<?php namespace Key\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKeyMapsLocations extends Migration
{
    public function up()
    {
        Schema::table('key_maps_locations', function($table)
        {
            $table->double('latitude', 16, 12)->after('model_type')->nullable();
            $table->double('longitude', 16, 12)->after('latitude')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('key_maps_locations', function($table)
        {
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
    }
}
