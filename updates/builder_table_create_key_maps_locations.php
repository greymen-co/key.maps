<?php namespace Key\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKeyMapsLocations extends Migration
{
    public function up()
    {
        Schema::create('key_maps_locations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('model_id')->nullable()->unsigned();
            $table->string('model_type', 255)->nullable();
            $table->text('data')->nullable();
            $table->index('model_id');
            $table->index('model_type');
        });
    }

    public function down()
    {
        Schema::dropIfExists('key_maps_locations');
    }
}