<?php namespace Key\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKeyMapsLocationModels4 extends Migration
{
    public function up()
    {
        Schema::table('key_maps_location_models', function($table)
        {
            $table->string('excluded_ids', 255)->after('popup_field')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('key_maps_location_models', function($table)
        {
            $table->dropColumn('excluded_ids');
        });
    }
}
