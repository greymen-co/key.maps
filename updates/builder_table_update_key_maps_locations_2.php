<?php namespace Key\Maps\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKeyMapsLocations2 extends Migration
{
    public function up()
    {
        Schema::table('key_maps_locations', function($table)
        {
            $table->json('data')->nullable()->unsigned(false)->default(null)->comment(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('key_maps_locations', function($table)
        {
            $table->text('data')->nullable()->unsigned(false)->default(null)->comment(null)->change();
        });
    }
}