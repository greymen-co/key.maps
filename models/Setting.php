<?php namespace Key\Maps\Models;

use Model;

/**
 * Class Setting
 * @package Key\Maps\Models
 */
class Setting extends Model {
    /** @var string[] */
    public $implement = ['System.Behaviors.SettingsModel'];

    /** @var string */
    public $settingsCode = 'key_maps_settings';

    /** @var string */
    public $settingsFields = 'fields.yaml';
}
