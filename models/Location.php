<?php namespace Key\Maps\Models;

use Model;

/**
 * Class Location
 * @package Key\Maps\Models
 */
class Location extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $fillable = ['model_id', 'model_type', 'latitude', 'longitude', 'data'];

    public $jsonable = ['data'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'key_maps_locations';

    /**
     * @var array Validation rules
     */
    public $rules = [];
}
