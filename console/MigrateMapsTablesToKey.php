<?php namespace Key\Maps\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

/**
 * MigrateMapsTablesToKey Command
 *
 * @link https://docs.octobercms.com/3.x/extend/console-commands.html
 */
class MigrateMapsTablesToKey extends Command
{
    /**
     * @var string signature for the console command.
     */
    protected $signature = 'key:migratemapstablestokey';

    /**
     * @var string description is the console command description
     */
    protected $description = 'Rename greymenco tables to Key';

    /**
     * handle executes the console command.
     */
    public function handle()
    {
        $backupDirectory = storage_path('app/backups');
        if (!file_exists($backupDirectory)) {
            mkdir($backupDirectory, 0755, true);
        }

        echo "If prompted for a password, please enter your database password.\nSame goes for other possible prompts.\n";

        $backupFileName = 'database_backup_keymaps_' . date('Y-m-d_H-i-s') . '.sql';
        $backupPath = $backupDirectory . '/' . $backupFileName;

        $tablesToBackup = [
            'greymen_maps_location_models',
            'greymen_maps_location',
        ];

        $tablesToRename = [
            'greymen_maps_location_models' => 'key_maps_location_models',
            'greymen_maps_location' => 'key_maps_location'
        ];

        $backupCommand = sprintf(
            'mysqldump -u%s -p%s %s %s > %s',
            env('DB_USERNAME'),
            env('DB_PASSWORD'),
            env('DB_DATABASE'),
            implode(' ', $tablesToBackup),
            $backupPath
        );
        exec($backupCommand);

        foreach ($tablesToRename as $oldTable => $newTable) {
            if (Schema::hasTable($oldTable)) {
                Schema::rename($oldTable, $newTable);
                $this->info("Table '$oldTable' renamed to '$newTable'.");
            } else {
                $this->error("Table '$oldTable' does not exist.");
            }
        }

        $this->info("Backup saved to: $backupPath");
    }
}
