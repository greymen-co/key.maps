<?php namespace Key\Maps\Components;

use Cms\Classes\ComponentBase;
use Key\Maps\Models\LocationModel;
use Key\Maps\Models\Setting;
use RainLab\Translate\Classes\Translator;
use Response;

class Mapbox extends ComponentBase
{
    /** @var bool */
    private static bool $spiderifierEnabled = true;

    public function componentDetails()
    {
        return [
            'name' => 'Mapbox Component',
            'description' => 'Mapbox with marked stores'
        ];
    }

    public function defineProperties()
    {
        return [
            'modelId' => [
                'title' => 'key.maps::lang.components.locations.model_id',
                'type' => 'dropdown',
                'placeholder' => 'Select a model',
                'options' => LocationModel::getModels(),
                'default' => 0,
                'required' => true,
            ],
            'locationId' => [
                'title' => 'key.maps::lang.components.locations.location_id',
                'type' => 'string',
                'placeholder' => 'key.maps::lang.components.locations.location_placeholder',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'key.maps::lang.components.locations.location_id_validation',
                'default' => 0,
            ],
            'searchDisabled' => [
                'title' => 'key.maps::lang.components.locations.disabled_search',
                'type' => 'checkbox',
                'default' => true,
            ],
            'refreshMarkersAfterFilter' => [
                'title' => 'key.maps::lang.components.locations.refresh_markers_after_filter',
                'type' => 'checkbox',
                'default' => true,
            ],
        ];
    }

    public function onGeoJSON()
    {
        $json = [
            'type' => 'FeatureCollection',
            'features' => [],
        ];
        $data = post();
        $params = \Input::get();
        $settings = (isset($data['mapboxSettings']) && is_array($data['mapboxSettings'])) ? $data['mapboxSettings'] : [];
        $mapsData = (isset($data['mapsData']) && is_array($data['mapsData'])) ? $data['mapsData'] : [];
        $modelId = (isset($mapsData['model_id']) && is_numeric($mapsData['model_id'])) ? intval($mapsData['model_id']) : 0;
        $locationId = (isset($mapsData['location_id']) && is_numeric($mapsData['location_id'])) ? intval($mapsData['location_id']) : 0;
        if ($locationId > 0) {
            $params['location_id'] = $locationId;
        }
        $model = LocationModel::find($modelId);
        if ($model === null) {
            return Response::json($json);
        }
        $locations = $model->model_type::getLocations($params, $settings);
        $appUrl = \Config::get('app.url');
        $popupSettings = [
            'view_page_label' => $model->view_page_label ?? trans('key.maps::lang.general.view_more'),
        ];
        foreach ($locations as $id => $location) {
            $popupSettings['url'] = (!empty($model->detail_page_url)) ? $appUrl . str_replace('[id]', $location->model_id, $model->detail_page_url) : '';
            $json['features'][] = [
                'type' => 'Feature',
                'properties' => [
                    'id' => $id,
                    'name' => $location->data['name'],
                    'popupHTML' => $this->renderPartial('@popup-content.htm', ['location' => $location, 'settings' => $popupSettings]),
                ],
                'geometry' => [
                    'type' => 'Point',
                    'coordinates' => [$location->data['longitude'], $location->data['latitude']],
                ],
            ];
        }
        return Response::json($json);
    }

    public function onTrue()
    {
        return true;
    }

    public function onRender()
    {
        $modelId = intval($this->property('modelId'));
        $locationId = intval($this->property('locationId'));
        $searchDisabled = boolval($this->property('searchDisabled'));
        $refreshMarkersAfterFilter = boolval($this->property('refreshMarkersAfterFilter'));
        $markerIcon = Setting::get('map_marker_icon');
        $markerIcon = (!empty($markerIcon)) ? '/storage/app/media' . $markerIcon : '/plugins/key/maps/assets/images/icon-marker.png';
        $markerClustererIcon = Setting::get('map_marker_clusterer_icon');
        $markerClustererIcon = (!empty($markerClustererIcon)) ? '/storage/app/media' . $markerClustererIcon : '/plugins/key/maps/assets/images/icon-marker-clusterer.png';
        $this->page['mapbox_settings'] = [
            'api_key' => Setting::get('mapbox_api_key'),
            'style_url' => Setting::get('mapbox_style_url'),
            'version' => Setting::get('mapbox_version'),
            'icons' => [
                'marker' => $markerIcon,
                'marker_clusterer' => [
                    'icon' => $markerClustererIcon,
                    'offsets' => [
                        'x' => Setting::get('map_marker_clusterer_icon_x_offset', 0),
                        'y' => Setting::get('map_marker_clusterer_icon_y_offset', 0),
                    ],
                ],
            ],
            'spiderifier_enabled' => self::$spiderifierEnabled,
            'popup_disabled' => ($locationId > 0),
            'search_disabled' => $searchDisabled,
            'refresh_markers_after_filter' => $refreshMarkersAfterFilter,
        ];
        $this->page['maps_data'] = [
            'model_id' => $modelId,
            'location_id' => $locationId,
        ];
    }

    public function onRun()
    {
        self::$spiderifierEnabled = boolval(Setting::get('map_enable_spiderifier', true));
        $this->addCss('/plugins/key/maps/assets/css/store-locator.css');
        if (self::$spiderifierEnabled) {
            $this->addJs('/plugins/key/maps/assets/js/lodash.min.js');
            $this->addJs('/plugins/key/maps/assets/js/spiderifier.js');
        }
        $this->addJs('/plugins/key/maps/assets/js/mapbox.js');
    }

    public function onOnlineStores()
    {
        return [
            '.online-stores' => $this->renderPartial('Mapbox::onlinestores.htm', ['showHeader' => true]),
        ];
    }

    public function onStoreLocator()
    {
        $data['showHeader'] = true;
        return [
            '#map' => $this->renderPartial('Mapbox::mapbox.htm', $data),
        ];
    }

    public function onMapResults()
    {
        $translator = Translator::instance();
        $activeLocale = $translator->getLocale();
        $stores = LocationModel::getGeoJSON();
        $data['stores'] = $stores['stores'];
        $data['error'] = $stores['error'];
        $data['geodata'] = json_encode($stores['geodata']);
        $data['zipcode'] = post('zip');
        $data['distance'] = post('distance');
        $data['country'] = post('country') ? post('country') : 'nl';
        return [
            '.results-inner' => $this->renderPartial('Mapbox::showresults.htm', $data)
        ];
    }

}
