<?php namespace Key\Maps\Tests\Models;

use Key\Maps\Models\LocationModel;
use PluginTestCase;

final class LocationTest extends PluginTestCase
{
    public function testEmpty(): array
    {
        $locations = [];
        $this->assertEmpty($locations);

        return $locations;
    }

    /**
     * @depends testEmpty
     */
    public function addLocation(array $locations) : array
    {
        array_push($locations, 'Amsterdam');
        $this->assertSame('Amsterdam', $locations[count($locations) - 1]);
        $this->assertNotEmpty($locations);

        return $locations;
    }
}

final class StackTest extends PluginTestCase
{

    public function testEmpty(): array
    {
        $stack = [];
        $this->assertEmpty($stack);

        return $stack;
    }

    /**
     * @depends testEmpty
     */
    public function testPush(array $stack): array
    {
        array_push($stack, 'foo');
        $this->assertSame('foo', $stack[count($stack) - 1]);
        $this->assertNotEmpty($stack);

        return $stack;
    }
}
